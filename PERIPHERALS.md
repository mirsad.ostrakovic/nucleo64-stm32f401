The most times in texts about embedded programming you will see word peripheral without any formal introduction to it. It's okay if you are already familiar with the world of embedded programming and you know what is peripheral. The formal introduction of such a broad term is very hard, so definition given here will be strictly related to our case.

A peripheral is a piece of embedded hardware which provides some specific functionality. In most cases, their functionality is related to communication and interaction with the outer world, as it is the case with SPI, UART, I2C, ADC etc. But there are also some peripherals whose only purpose is to enhance the performance and robustness of the system, for example, DMA or watchdog timer. The list of all peripherals with detail description of every of it you can find in the reference manual of your MCU, in our case it is RM0368. 

It is hard to define the entry point because you will need to know several different things to be able to do anything. So I will not be strict with it here. The main goal is to make drivers for GPIO peripheral by the end of this chapter. What is GPIO? GPIO is abbreviation of General Purpose Input Output. This is the name given by STM, but the other producers also have a similar name for peripheral with the same purpose. GPIO is peripheral which handle the state of the pins of the MCU. There are a few different modes of operation that can be specified for each GPIO pin: input mode, output mode, alternate function mode and analog mode. For the beginning, a good understanding of input mode and output mode will be enough. Nevertheless, the other ones will be also briefly introduced.

Inside STM MCU's GPIO pins are grouped into GPIO ports. There are a few reasons for that which I will list below, but it is good to know that this list is not given by priority because it is hard to choose which one has better arguments.

It is easier to handle the clock of GPIO pins if they are grouped into ports. For example, this way of implementation enables you to turn on and off the clock for a group of pins at the same time.

If GPIO pins are grouped, you are able to change configuration and state of a few pins at the same time. 

It is a good compromise which eases a job for producer of MCU and at the same time enables you to write generic drivers because of specific
layout of registers.
