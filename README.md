# Introduction

Intention of this document is to provide good foundation of embedded sofware development to individuals who
have previous programming knowledge, but who have not yet tried their hands at this field.
So far, I have worked with MCUs from several different producers and really there are no huge difference between them. More or less, for the same amount of money you can get MCUs with similar performances and set of periferals from different producers. 
However, I realized from experience that STM32 line of MCUs is the best sutiable for the begginers in this field. The main reason of that is
very good documentation, with a lot of details. Because of that, I choosed it for this tutorial.

After that, I had dilemma about which evaluation board should I use. A thing that you should keep in the mind is that, a microcontroller gets it's worth only when it is connect to other hardware components and when it is able to do some measurements and control operations or perform communication with other devices or parts of the system. In order to demonstrate the most of capabilities of MCU, we will need a lot of additional hardware components. The first option is to have them integrated onto evaluation board alongside with the MCU. 
The other one is to have minimalistic evaluation board which only contains MCU with few other necessary components and later to buy external hardware modules which will provide some additional functionality to the system and enable you to test some capabilities of the MCU and learn more about it's peripherals.
Which option is better and more sutiable depends on your current circumstances. 
Evaluation boards with additionaly embedded hardware components as sensors, displays, some communication ports and so on are better from standpoint that you have all in one package for testing and developing, which hardware desing passed all necessary tests and verification procedures. In this way you can avoid all issues related to hardware design as for example are EMI or parasitic capacitance and inductance in a circuit.
However, all these benefits come with the greater price, which can be few times higher compared to the price of some basic evaluation boards. On you is to choose wisely. My opinion is that you should use more advanced version of evaluation boards especially if you don't have a lot of knowledge about hardware design, so you can be always sure that problems which arise in your project are there due to a software bug.
However, if you don't want to spend more than few tens of dollars or if your budget doesn't allow you to do that, you can still buy a basic evaluation board at the beggining and then with with the course of time buy additional hardware modules and components which will enable you to implement new fuctionalities in your project.

Because of all these things, I decided to do turtorial in parallel for two boards. The first board will be one from the cheapest series of STM32 evaluation boards - STM32 NUCLEO series, and the second board will be from STM32 DISCOVERY series of the boards, more expensive one, but still not at the high end of the price and capabilites range. The board which will be used are NUCLEO64 STM32F401 and 32L4R9IDISCOVERY.

To compleat discussion of STM32 eveluation boards, I will spend there some words on EVAL series of STM32 evaluation boards. These series is the most expensive one and it gives to product developers the most flexibility and capabilities for early product prototyping. They are designed with an idea that they should cover all cases for which given MCU can be used. 
As a newbie, you shouldn't even consider to use boards like these ones, especially at the very beginning of your journey. Even professionals don't take them into for early prototyping if they don't have to and if some lower budget board meets product requirements.


At the end of this chapter, I want to make a little intro about timeline of product developing, which especially can be usefull for newbie in this field.
At the beginning, when roughly all product requirements are known, system architects will choose MCU(s) which meet(s) all of the given product requirements. 
After that, appropriate evaluation board for given MCU, which will be used for early product prototyping, will be found. At this point, real procces of product developing starts. 
At the time when the early prototype will be finished and the final set of peripherals, which will be used, will be known, hardware developer will start with designing and developing of the end product PCB board. Starting from this phase process od hardware and sofware development are done iteratively and cyclically. There are many reasons for that, but there will mention only few of them: 
- Possible errors in the PCB design, especially ones which are not obvious and only manifest rarely at some special circumstances. For example, if there are 
  some, so far unknown, hardware error or if there exists EMI problem which only occurs when more hardware components are simulationously involved in 
  some kind of communication.
- Changes in the product specification, due to expanding capabilities or due cost cutting of the end product.
- Changes in the system architecture, which can occurs if some hardware/software solution doesn't meet wanted performances.
- Oversights and mistakes in software development, which can cause software and/or hardware redesign of some parts of the system.

 




# NUCLEO64 STM32F401 and 32L4R9IDISCOVERY



Resources about this board can be found on this web page:
https://www.st.com/en/microcontrollers-microprocessors/stm32f401.html#resource

The most important resoruces are: 
 - Reference manual, document RM0368
 - Datasheet, document DS9716 or DS10086, depending of the version of the board
 - Errata Sheet, document ES0299 or ES0222, depending of the version of the board

Additional information about the board and it's peripherals can be found in 
Application notes.

Reference manual contains description of peripherals, their registers and their
mode of operation.

At the beggining, the most important thing about Datasheet is that it contains the
table which describes mapping of GPIO pin to peripherals input/output (AF - alternate function).

Download STM32CubeF4 (STM32Cube MCU Package for STM32F4 series) from the page (you may need to register to the web page):
https://www.st.com/en/embedded-software/stm32cubef4.html#overview

STM32CubeF4 contains libraries of several different API, of which the most important is HAL API. Briefly to say, 
HAL API implements drivers for peripherals of the MCU. It hides access and programming of registers in some way.
My sugestion is that at the beginning, you should try to raw program periferals (via direct programming of registers).
Learning curve will be steep at the beggining, but this way of learning will greatly improve you deep knowledge and 
ease understanding of the other things later.


After you download SMT32CubeF4 package, you need to unzip it in you working directory. In the rest of this text, a path  to the working directory, on your file system,
will be mark as WORKING_DIR. Also, it will be implied that you use terminal enviroment under Linux, or some similar UNIX like operating system.

To setup enviroment:

1) Change your current working directory(CWD) to WORKING_DIR, in my case WORKING_DIR is '~/STM_tut'. In your WORKING_DIR, you need to create folder 'project_template'. You
   can do it by typing 'mkdir project_template'.

2) Change you current working directory(CWD) to 'WORKING_DIR/STM32Cube_FW_F4_V1.25.0/Projects/STM32F401RE-Nucleo/Templates/SW4STM32'. Maybe it be a case, that you will have 
   different version of the STM32CubeF4 compared to one that I downloaded. So 'STM32Cube_FW_F4_V1.25.0' part of the path can differentiate in the version number. Because of
   that I suggest to you to use TAB in terminal to autocompleat path, it will ease your life a lot.

3) In this directory you will find file 'startup_stm32f401xe.s' and also another directory 'STM32F4xx-Nucleo' in which you will find file 'STM32F401VEHx_FLASH.ld'. You need
   to copy both of this files into your project template folder. You can do it with terminal cp command(program). You need to type this:
   
   ```bash
      cp startup_stm32f401xe.s WORKING_DIR/project_template/
      cp STM32F4xx-Nucleo/STM32F401VEHx_FLASH.ld  WORKING_DIR/project_template/
    ```
      
      In my case these command were:
    
    ```
      cp startup_stm32f401xe.s ~/STM_tut/project_template/ 
      cp STM32F4xx-Nucleo/STM32F401VEHx_FLASH.ld  ~/STM_tut/project_template/
    ```

4) Change CWD to 'WORKING_DIR/STM_tut/STM32Cube_FW_F4_V1.25.0/Drivers/CMSIS/Include' and then copy file 'core_cm4.h' to your project template folder. You can do this 
   with the command:
    
    ```  
      cp core_cm4.h WORKING_DIR/project_template/
    ```
      In my case command were:
    ```  
      cp core_cm4.h ~/STM_tut/project_template/
    ```  
      This file is not crucial for compiling of a project, because it will be included anyway. But, it is imporant for you, because in it there are definitions of functions
      which work with NVIC(nested vector interrupt controller). So, in the case that you will need it, you will have easy access to it.
      
5)  Change CWD to 'WORKING_DIR/STM32Cube_FW_F4_V1.25.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include$, in my case it was '~/STM_tut/STM32Cube_FW_F4_V1.25.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include'.
    Copy file 'stmf401xe.h' to project template, with the command:
    ```
      cp stm32f401xe.h WORKING_DIR/project_template/
    ```
      In my case command were:
    ```  
      cp stm32f401xe.h ~/STM_tut/project_template/
    ```  
      This file is very imporant because it contains macro definitions of pointers to the registers of the all peripherals. You should get to know with it very well, because you will use it
      intensively.


6) Change CWD to 'WORKING_DIR/STM32Cube_FW_F4_V1.25.0/Drivers/CMSIS/Device/ST/STM32F4xx/Source/Templates', in my case it was '~/STM_tut/STM32Cube_FW_F4_V1.25.0/Drivers/CMSIS/Device/ST/STM32F4xx/Source/Templates'.
   Copy 'system_stm32f4xx.c' to project template folder, with the command:
    ```  
      cp system_stm32f4xx.c WORKING_DIR/project_template/
    ```
    
      In my case command were:
   ```
      cp system_stm32f4xx.c ~/STM_tut/project_template/
    ```
    
    Inside this folder there are also few other folders, which with the name correnspond to the version of compiler that you will use for compilation. In our case, we will use gcc. If you change 
    CWD to this folder with the command 'cd gcc' and then type command 'ls' you will see that this folder contains files with the startup code for different versions of STM32F4 series of MCU. If
    you will need different startup file, you know where you can find it.

   

**OPENOCD**

Before you will be able to run your code on the board, you need a way to download binary (executable) on it. The way you can and must do it is via help of a debug probe. Debug probe is a piece of hardware 
which main purpouse is to provide a way for debuging and flashing of a binary on MCUs. 

Every debug probe contains 2 interfaces, one to the MCU (CPU) and another one to the PC (notebook). It gets commands from PC inteface and according to them set own internal states or/and issues
commands at the MCU inteface.

ARM architecture compliant MCUs (CPUs) implement JTAG, SWD or both of these protocols, for communication with debugers and programers. In order to be able to establish communication
with the ARM MCU, your debug probe need to support at the least one of the interfaces (protocols) which are implemented by the MCU you use.

On the other side of communication chain, every debug probe implement it's own interface for communication with it. The most debug probes use USB as the physical interface for communication,
however semantic and syntax of protocol (commands) above it differes between each one.

Nowdays, the most used debug probe is SEGGER J-Link, but as a beginner probably you will not use it. Reason of that is, that j-link is the tool for professionals and it's price is 
very high and unaffordable for an avarage tech enthusiast. If you are a student, there is possiblity to get J-Link EDU version for relatively cheap price. Of course, this version has limited
capabilites compared to the other expesive brothers (J-Link Ultra+ or J-Link Pro), but for a student use case it is more than enough.

Luckly for us, these days the most of MCU procuders think about begginers and lack of the their willigness to spend a lot of money just to dive in the world of embedded systems. Because of that, 
the most of them integrates cheap debug probe at the MCU test board. So, the only think you will probably need is an USB cable to connect these board with your PC (notebook).



NUCLEO-F401RE integrates the ST-LINK debugger/programmer, which can be connected to the PC (notebook) via USB. So, you don't need to worry about buying external debug probe.
One more thing that you will need, before you can start to test and debug code on the board, is an application which can talk to the ST-LINK.


One of the most used applications to speak with debug probes is openocd.
From my point of view, openocd is a great tool becuase it supports many debug probes and enables an user of it to easly exchange between ones without need to change MCU configuration script. 
Also, openocd comes with predefines configuration scripts for a lof of MCUs and MCU's test boards, so in the most cases it can be used as plug and play solution. 
It is worth to mention that openocd is free to use tool, even for corporations and because of that is widely adopted in the community. 

(For example with openocd you can exhange from ST-LINK to J-Link with exchanging just one parametar in call to openocd.)

Move to the project root directory with command:
```
  cd WORKDING_DIR
```

In my case it was:
```
  cd ~/STM_tut
```

You can download openocd installation via git with the terminal command: git clone https://git.code.sf.net/p/openocd/code openocd-code


You need to execute this commands to install dependent libraries, before you can proceed:
```
  sudo apt-get install make
  sudo apt-get install libtool
  sudo apt-get install pkg-config
  sudo apt-get install autoconf
  sudo apt-get install automake
  sudo apt-get install texinfo
  sudo apt-get install libusb-1.0
```

On the web page: https://sourceforge.net/p/openocd/code, you can find instructions how to compile it. Basically, you need to run this instruction in terminal, from inside of openocd directory:
```
   ./bootstrap
   ./configure
    make
    sudo make install
```
In the case of a problem chech section 'OpenOCD Dependencies', maybe you will need to install some of the dependency library. So for example, if dependency library is 'autoconf', you can install
it with the command: 'sudo apt-get install autoconf'.


The commands you should run are:
```
  sudo apt-get install make
  sudo apt-get install libtool
  sudo apt-get install pkg-config
  sudo apt-get install autoconf
  sudo apt-get install automake
  sudo apt-get install texinfo
  sudo apt-get install libusb-1.0
```

After you finish with compiling you can find openocd executable inside src directory. 

After you connect your NUCLE-F401RE board with your PC, you can reposition yourself inside tcl directory in openocd folder and then execute
command: 'openocd -f board/st_nucleo_f4.cfg'. If everything work well, you will be able to connect to your nucleo board.

```
  openocd -f board/st_nucleo_f4.cfg
```


You need to download arm-none-eabi GNU Arm Embedded Toolchain from the page:
https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm/downloads
You should select version appropriate for you operating system, for most of you it will be Linux x86_64 Tarball. After it, you need to extract it in your WORKING_DIR. After extracting, in my
case, name of the folder where the data are extracted is 'gcc-arm-none-eabi-9-2019-q4-major'. Inside it, you can find folder 'bin' which contains apps as: 
  - C compiler (arm-none-eab-gcc), 
  - debugging client (arm-none-eabi-gdb),
  - linker (arm-none-eabi-ld).
These apps are necessary for development of applications for any arm based MCU/CPU.

Before you proceed, you need to change name of the folder with your GNU Arm Embedded Toolchain from whatever it is to 'gcc-arm-none-eabi'. In my case, I did it with command:
'mv gcc-arm-none-eabi-9-2019-q4-major/ gcc-arm-none-eabi'.

Now you can call this application these commands:

```
  WORKING_DIR/gcc-arm-none-eabi/bin/arm-none-eabi-gdb
  WORKING_DIR/gcc-arm-none-eabi/bin/arm-none-eabi-gcc
  WORKING_DIR/gcc-arm-none-eabi/bin/arm-none-eabi-ld
```
In my case:
```
  ~/STM_tut/gcc-arm-none-eabi/bin/arm-none-eabi-gdb
  ~/STM_tut/gcc-arm-none-eabi/bin/arm-none-eabi-gcc
  ~/STM_tut/gcc-arm-none-eabi/bin/arm-none-eabi-ld
```

Now, when we finished with the installation of openocd and GNU Arm Embedded Toolchain, we can try to connected to our board. You need to open 2 instance of terminal in Linux.
In the first one you will need to execute command which will run opeoncd debugging server: 

```
openocd -f board/st_nucleo_f4.cfg
```

Apart from running openocd debugging server, we also specified openocd configuration script for our board. So, the openocd instance will connect to the Nucleo board in the
background without any additional help.
After starting you will receive information on which port debugging server is running, in the most cases it will be 3333.

In the second instance of terminal, you need to run command 'WORKING_DIR/gcc-arm-none-eabi/bin/arm-none-eabi-gdb', after which you will start debugging client. At the beggining your
debugging client is not connected to any gdb server, so you need to connect it to your local one with gdb command 'target remote localhost:3333'. For you who lack a knowledge of computer's network,
'localhost:3333' means that target, for the gdb client, is located on you machine (localy) on the port 3333.

If you run this successfully, then you can proceed to the next chapter.


More information about openocd you can find in openocd user manual, on the page: http://openocd.org/doc.

**TEST YOUR BOARD WITH OPENOCD**

To automate process of connecting gdb client to gdb server on startup, gdb client enables you to specify text file with gdb commands which will be executed on startup. You can do
it explicitly by stating flag '-x' together with the path to the init file in the form '-x  PATH_TO_GDB_INIT'. Or you can specify .gdbinit file, for which gdb client search automatically on startup
on some predefined locations in the file system. 
We will prefere explicitly way of stating file with gdb commands.

In the top of your WORKING_DIR create folder 'gdbinit' and inside it create file with the name 'test.gdbinit'.

```
cd WORKDING_DIR
mdkir gdbinit
cd gdbinit
touch test.gdbinit
```

After that, open file 'test.gdbinit' and add line 'target remote localhost:3333' and close it.
Now you can repeat proces of staring openocd (gdb server) and arm-none-eabi-gdb (gdb client) in separted instances of terminal, but now you will call gdb client with command:

```
WORKING_DIR/gcc-arm-none-eabi/bin/arm-none-eabi-gdb -x WORKING_DIR/gdbinit/test.gdbinit
```

In my case it was:

```
~/STM_tut/gcc-arm-none-eabi/bin/arm-none-eabi-gdb -x ~/STM_tut/gdbinit/test.gdbinit
```

Here I will make short introduction about debugging on ARM architectur CPU/MCU. ARM based CPUs/MCUs support 2 protocols/interfaces for debugging. First one is JTAG, it is ubiquitous interface/protocol for debugging and many CPU architectures support it. On the other hand, SWD is ARM proprietary interface/protocol for debugging and it is implemented by the almost all ARM compliant CPUs/MCUs (99.99%). The main difference and the advantange of SWD compared to JTAG, is that it uses less wires/pins to establish communication with CPUs/MCUs. So, in the most cases it will be used to establish communication with our MCU. Inside every ARM based CPUs/MCUs, there is special piece of hardware inside, called DAP (Debug Access Port), which enables you to connect to CPU/CPU with the purpose of debugging. DAP is splited in two part, first one is called DP (Debug Port) and there has to be one and only one DP for the whole CPU/MCU. Other part is called AP (Access Port) and there has to be at the least one AP in the CPU/MCU, but there is also possibility that CPU/MCU contains few of them. The situation in which a CPU will probably have few APs is when it contains multiple cores inside. Multicore architecure is dramatically more complicated compared to single core CPU/MCU architecture, so there are justified needs to have more APs. Nevertheless, in our case we will only have one DAP and one AP. DAP acts like gateway, from the one end it enables you to connect debug probe which support SWD or JTAG protocol/interface, and from the other end enables you to connect to one AP at the same time. AP is piece of hardware which enables you to connect to CPU/MCU core debug unit, trace unit, performance tracking unit and similar. The most important unit for us is debug unit, it enables us to read and change values of registers and memory locations, halt and resume execution of program manually or automatically via help of break points, and many more things.

The most imporant thing for the next example is ability to write to memory and read from it via help of debug unit. But before we can proceed, I will make a little digression. 
Every piece of hardware is controller via registers, so the same situation is with the hardware inside our MCU. 
A register is piece of memory which is located inside hardware component and it in some way determines the current configuration and mode of operation of that hardware component. So, if we know in which way all of the registers determine the current configuration, we are able to set values of them and establish control over that piece of hardware. The most common case is that in each register, values of groups of bits define aspect of operation of that hardware and usually we have to change few values inside few registers to configure the hardware. Not to forget that there are also registers inside a hardware which provide status information to the outter world and they are called status registers.
Registers are mapped to predefined memory location in each and every MCU. Producers of CPUs/MCUs describe registers and give to us addresses to which they are mapped inside documentation. Usually, memory layout is constructed in the way that enables developers to write generic drivers. To conclude, everything that we need to control some piece of hardware is to know to which location which value we should write, and of course to know from which location which value we should read.

Now, when we know all these things, we know that we are able to control peripherals of MCU without help of the processor core. Indeed, the processor core can be shut down or halted and we will still be able to initialize ane control every peripheral inside MCU and below we will demonstrate this via help of openocd. This fact is imporant for understanding the 
more advanced concepts of CPUs/MCUs programming. For example, you are able to utilize DMA peripheral for copying data from RAM memory to some other peripheral while CPU is in sleep mode, this way you will tremendously reduce power consumption.

Openocd implements Tcl scripting language together openocd API for it. With the help of it, you are able to setup configuration of DP and AP and to connect to debug unit of the desired core. After that you are able to use generic openocd API commands for writing to memory locations or reading from them. You are also able to read and change values of registers via help of that API. So, you have everything that you need to dive into this world. Please, don't be scared of the part about configuration of DAP, in 99% cases openocd provides predefined script for you core, so everything that you will need is to specify name of this script at the time of the call of openocd.


Detailed description of Tcl scripting language you can find online, however the most imporant things for understanding this script code will be inserted into it as comments.


```
# This procedure is wrapper for writting 32 bits value to memory. Address has to be aligned.
proc writeMem32 {addr value} {
  mww $addr $value
}

# This procedure is wrapper for reading 32 bits value from memory. Address has to be aligned.
proc readMem32 {addr} {
  set tmpArray(0) 0x0
  mem2array tmpArray 32 $addr 1
  return $tmpArray(0)
}


# Keyword 'variable' in Tcl denotes global variable. Global variables can be referenced by adding :: prefix to their name.
# For example if the name given to the global variable is PERIPH_BASE, in the rest of the script code you will reference to it with
# ::PERIPH_BASE.

# Tcl supports arithmetical expressions. However, you need to specify explicitly that you want for some text to be processed as arithmetical
# expression. In the other way, Tcl will process it as a plain text. The way to do it is with this Tcl text's form: [expr ARITMETICAL_EXPRESSION].

# All of the variable's names and values specified below are taken over from header file 'stmf401xe.h'. This header file will be very imporant
# for your future advanture into the world of embedded programming, so may should look at it briefly.
variable PERIPH_BASE          0x40000000

variable APB1PERIPH_BASE      [expr $::PERIPH_BASE]
variable APB2PERIPH_BASE      [expr $::PERIPH_BASE + 0x00010000]
variable AHB1PERIPH_BASE      [expr $::PERIPH_BASE + 0x00020000]
variable AHB2PERIPH_BASE      [expr $::PERIPH_BASE + 0x10000000]

variable RCC_BASE             [expr $::AHB1PERIPH_BASE + 0x3800]
variable RCC                  [expr $::RCC_BASE]

variable GPIOA_BASE            [expr $::AHB1PERIPH_BASE + 0x0000]
variable GPIOB_BASE            [expr $::AHB1PERIPH_BASE + 0x0400]
variable GPIOC_BASE            [expr $::AHB1PERIPH_BASE + 0x0800]
variable GPIOD_BASE            [expr $::AHB1PERIPH_BASE + 0x0C00]
variable GPIOE_BASE            [expr $::AHB1PERIPH_BASE + 0x1000]
variable GPIOH_BASE            [expr $::AHB1PERIPH_BASE + 0x1C00]

variable GPIOA                [expr $::GPIOA_BASE]
variable GPIOB                [expr $::GPIOB_BASE]
variable GPIOC                [expr $::GPIOC_BASE]
variable GPIOD                [expr $::GPIOD_BASE]
variable GPIOE                [expr $::GPIOE_BASE]
variable GPIOH                [expr $::GPIOH_BASE]


# For the values of offsets, please look at the structure 'RCC_TypeDef'.
 variable AHB1ENR_OFFSET       0x30
 variable AHB2ENR_OFFSET       0x34
 variable AHB3ENR_OFFSET       0x38 
 variable APB1ENR_OFFSET       0x40
 variable APB2ENR_OFFSET       0x44 


variable RCC_AHB1ENR_GPIOAEN_Pos            0                                
variable RCC_AHB1ENR_GPIOAEN_Msk            [expr 0x1 << $::RCC_AHB1ENR_GPIOAEN_Pos]
variable RCC_AHB1ENR_GPIOAEN                [expr $::RCC_AHB1ENR_GPIOAEN_Msk]             
variable RCC_AHB1ENR_GPIOBEN_Pos            1                                
variable RCC_AHB1ENR_GPIOBEN_Msk            [expr 0x1 << $::RCC_AHB1ENR_GPIOBEN_Pos]
variable RCC_AHB1ENR_GPIOBEN                [expr $::RCC_AHB1ENR_GPIOBEN_Msk]             
variable RCC_AHB1ENR_GPIOCEN_Pos            2                                
variable RCC_AHB1ENR_GPIOCEN_Msk            [expr 0x1 << $::RCC_AHB1ENR_GPIOCEN_Pos]
variable RCC_AHB1ENR_GPIOCEN                [expr ::RCC_AHB1ENR_GPIOCEN_Msk]             
variable RCC_AHB1ENR_GPIODEN_Pos            3                                
variable RCC_AHB1ENR_GPIODEN_Msk            [expr 0x1 << $::RCC_AHB1ENR_GPIODEN_Pos]
variable RCC_AHB1ENR_GPIODEN                [expr $::RCC_AHB1ENR_GPIODEN_Msk]             
variable RCC_AHB1ENR_GPIOEEN_Pos            4                                
variable RCC_AHB1ENR_GPIOEEN_Msk            [expr 0x1 << $::RCC_AHB1ENR_GPIOEEN_Pos]
variable RCC_AHB1ENR_GPIOEEN                [expr $::RCC_AHB1ENR_GPIOEEN_Msk]             
variable RCC_AHB1ENR_GPIOHEN_Pos            7                                
variable RCC_AHB1ENR_GPIOHEN_Msk            [expr 0x1 << $::RCC_AHB1ENR_GPIOHEN_Pos]
variable RCC_AHB1ENR_GPIOHEN                [expr $::RCC_AHB1ENR_GPIOHEN_Msk]           


 # For the values of offsets, please look at the structure 'GPIO_TypeDef'.
variable GPIO_MODER_OFFSET         0x0
variable GPIO_OTYPER_OFFSET        0x4
variable GPIO_OSPEEDR_OFFSET       0x8
variable GPIO_PUPDR_OFFSET	   0xC
variable GPIO_IDR_OFFSET	   0x10
variable GPIO_ODR_OFFSET	   0x14
variable GPIO_BSRR_OFFSET          0x18
variable GPIO_LCKR_OFFSET	   0x1C
variable GPIO_AFR_0_OFFSET         0x20
variable GPIO_AFR_1_OFFSET         0x24

variable GPIO_MODER_MODER4_Pos            8                                 
variable GPIO_MODER_MODER4_Msk            [expr 0x3 << $::GPIO_MODER_MODER4_Pos]
variable GPIO_MODER_MODER4                [expr $::GPIO_MODER_MODER4_Msk]                 
variable GPIO_MODER_MODER4_0              [expr 0x1 << $::GPIO_MODER_MODER4_Pos]
variable GPIO_MODER_MODER4_1              [expr 0x2 << $::GPIO_MODER_MODER4_Pos]
variable GPIO_MODER_MODER5_Pos            10                                
variable GPIO_MODER_MODER5_Msk            [expr 0x3 << $::GPIO_MODER_MODER5_Pos]
variable GPIO_MODER_MODER5                [expr $::GPIO_MODER_MODER5_Msk]                 
variable GPIO_MODER_MODER5_0              [expr 0x1 << $::GPIO_MODER_MODER5_Pos]
variable GPIO_MODER_MODER5_1              [expr 0x2 << $::GPIO_MODER_MODER5_Pos]


# Please look at 'UM1724' page 66.
# GREEN LED => PA5


proc initLED {} {

  # Turn on clock signal for GPIOA port.
  # It first reads the value of RCC_AHB1ENR register, then modify it in the desired way, and after that updates it.
  set regAddr [expr $::RCC + $::RCC_AHB1ENR_OFFSET] 
  set regVal [readMem32 $regAddr]
  set regVal [expr $regVal | $::RCC_AHB1ENR_GPIOAEN] 
  writeMem32 $regAddr $regVal

  # Set mode of pin 5 of GPIOA port to be output.
  set regAddr [expr $::GPIOA + $::GPIO_MODER_OFFSET]
  set regVal  [readMem32 $regAddr]
  set regVal  [expr $regVal & (~ $::GPIO_MODER_MODE5_Msk)]	
  set regVal  [expr $regVal | $::GPIO_MODER_MODE5_0] 
  writeMem32 $regAddr $regVal
}


proc turnOnLED {} {
  
  # Set the output value of GPIOA port pin 5 to be 1.
  set regAddr [expr $::GPIOA + $::GPIO_ODR_OFFSET]
  set regVal  [readMem32 $regAddr]
  set regVal  [expr $regVal | (1 << 4)]
  writeMem32 $regAddr $regVal
}

proc turnOffLED {} {
  # Reet the output value of GPIOA port pin 5 to be 0.
  set regAddr [expr $::GPIOA + $::GPIO_ODR_OFFSET]
  set regVal  [readMem32 $regAddr] 
  set regVal  [expr $regVal & ~(1 << 4)]	
  writeMem32 $regAddr $regVal
}

```

You need to create file 'STM32_NUCLEO_F401RE_test.openocd' in the WORKING_DIR with command touch.

```
touch WORKING_DIR/STM32_NUCLEO_F401RE_test.openocd
```
After that you need to open this file and copy script code given above into it. When you finish this procedure, you will be able to specify 
this script in addition to the initialization script at the time of starting of openocd. 

```
openocd -f board/st_nucleo_f4.cfg -f WORKING_DIR/STM32_NUCLEO_F401RE_test.openocd
```

After that, you will be able to call procedures:
'initLED', 'turnOnLED' and 'turnOffLED' inside you gdb client with:

```
monitor initLED
monitor turnOnLED
monitor turnOffLED
```
